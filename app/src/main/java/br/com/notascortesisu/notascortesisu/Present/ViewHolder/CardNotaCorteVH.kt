package br.com.notascortesisu.notascortesisu.Present.ViewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import br.com.notascortesisu.notascortesisu.Model.NotaCorte
import kotlinx.android.synthetic.main.notas_corte_item.view.*
import kotlinx.android.synthetic.main.notas_curso_item.view.*

class CardNotaCorteVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val notaModalidade = itemView.textView_nota_corte
    private val modalidade = itemView.textView_modalidade

    fun bindView(notaCorte: NotaCorte){
        notaModalidade.text = notaCorte.nota.toString().replace(".", ",")
        modalidade.text = notaCorte.modalidade
    }

}