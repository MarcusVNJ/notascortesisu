package br.com.notascortesisu.notascortesisu.Present.CardView

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.notascortesisu.notascortesisu.Model.Curso
import br.com.notascortesisu.notascortesisu.Present.ViewHolder.CardCursoVH
import br.com.notascortesisu.notascortesisu.R
import br.com.notascortesisu.notascortesisu.View.DetalhesNotaCorteActivity

class CursoAdapter(private val context: Context, private var listCardCusos: MutableList<Curso>) : RecyclerView.Adapter<CardCursoVH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardCursoVH {
        val view = LayoutInflater.from(context).inflate(R.layout.notas_curso_item,parent,false)
        return CardCursoVH(view);
    }

    override fun getItemCount() = listCardCusos.size

    override fun onBindViewHolder(holder: CardCursoVH, position: Int) {
        holder.bindView(listCardCusos[position])

        holder.itemView.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v : View){
                val intent = Intent(context,DetalhesNotaCorteActivity::class.java)
                val bundle = ActivityOptionsCompat.makeScaleUpAnimation(v,0,0,v.width,v.height).toBundle()
                v.context.startActivity(intent,bundle)

            }
        })
    }
}