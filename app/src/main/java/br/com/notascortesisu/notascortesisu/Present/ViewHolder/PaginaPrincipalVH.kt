package br.com.notascortesisu.notascortesisu.Present.ViewHolder

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import br.com.notascortesisu.notascortesisu.R


class PaginaPrincipalVH (activity: AppCompatActivity){

    private var toolbar: Toolbar

    init {
        this.toolbar = activity.findViewById(R.id.tool_bar)
        bindToolBar(activity)
    }

    fun bindToolBar(activity: AppCompatActivity){
        activity.setSupportActionBar(toolbar)
    }
}