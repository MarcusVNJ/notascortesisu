package br.com.notascortesisu.notascortesisu.View


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.notascortesisu.notascortesisu.Present.PaginaPrincipalPst
import br.com.notascortesisu.notascortesisu.R


class PaginaPrincipalActivity : AppCompatActivity() {

    private lateinit var pagPrincipalPsr : PaginaPrincipalPst

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pagina_principal)

        pagPrincipalPsr = PaginaPrincipalPst(this)
    }
}
