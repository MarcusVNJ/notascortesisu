package br.com.notascortesisu.notascortesisu.Present

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import br.com.notascortesisu.notascortesisu.Model.NotaCorte
import br.com.notascortesisu.notascortesisu.Present.CardView.NotaCorteAdapter
import br.com.notascortesisu.notascortesisu.R


class DetalhesNotaCortePst(activity: AppCompatActivity){

    private var recyclerViewNotasCorte: RecyclerView
    var toolbar : Toolbar
    private var notasCorteList: MutableList<NotaCorte> = mutableListOf(NotaCorte(707.88f,"Ampla Concorrência"),NotaCorte(670.96f,"Candidatos autodeclarados pretos, pardos ou indígenas que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
          NotaCorte(660.18f, "Candidatos autodeclarados pretos, pardos ou indígenas, com renda familiar bruta per capita igual ou inferior a 1,5 salário mínimo e que tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
          NotaCorte(658.14f,"Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
          NotaCorte(675.06f, "Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(707.88f,"Ampla Concorrência"),NotaCorte(670.96f,"Candidatos autodeclarados pretos, pardos ou indígenas que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(660.18f, "Candidatos autodeclarados pretos, pardos ou indígenas, com renda familiar bruta per capita igual ou inferior a 1,5 salário mínimo e que tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(658.14f,"Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(675.06f, "Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(707.88f,"Ampla Concorrência"),NotaCorte(670.96f,"Candidatos autodeclarados pretos, pardos ou indígenas que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(660.18f, "Candidatos autodeclarados pretos, pardos ou indígenas, com renda familiar bruta per capita igual ou inferior a 1,5 salário mínimo e que tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(658.14f,"Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(675.06f, "Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(707.88f,"Ampla Concorrência"),NotaCorte(670.96f,"Candidatos autodeclarados pretos, pardos ou indígenas que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(660.18f, "Candidatos autodeclarados pretos, pardos ou indígenas, com renda familiar bruta per capita igual ou inferior a 1,5 salário mínimo e que tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(658.14f,"Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(675.06f, "Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(707.88f,"Ampla Concorrência"),NotaCorte(670.96f,"Candidatos autodeclarados pretos, pardos ou indígenas que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(660.18f, "Candidatos autodeclarados pretos, pardos ou indígenas, com renda familiar bruta per capita igual ou inferior a 1,5 salário mínimo e que tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(658.14f,"Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(675.06f, "Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(707.88f,"Ampla Concorrência"),NotaCorte(670.96f,"Candidatos autodeclarados pretos, pardos ou indígenas que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(660.18f, "Candidatos autodeclarados pretos, pardos ou indígenas, com renda familiar bruta per capita igual ou inferior a 1,5 salário mínimo e que tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(658.14f,"Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."),
            NotaCorte(675.06f, "Candidatos que, independentemente da renda (art. 14, II, Portaria Normativa nº 18/2012), tenham cursado integralmente o ensino médio em escolas públicas (Lei nº 12.711/2012)."))
    private var notaCorteAdapter: NotaCorteAdapter

    init {
        toolbar = activity.findViewById(R.id.toolbar_pag_detalhes)
        activity.setSupportActionBar(toolbar)

        this.recyclerViewNotasCorte = activity.findViewById(R.id.listaNotas)
        this.notaCorteAdapter = NotaCorteAdapter (activity.applicationContext,notasCorteList)
        this.recyclerViewNotasCorte.adapter = notaCorteAdapter
        this.recyclerViewNotasCorte.layoutManager = LinearLayoutManager(activity.applicationContext)
        this.recyclerViewNotasCorte.smoothScrollToPosition(notasCorteList.size)

    }

}