package br.com.notascortesisu.notascortesisu.Present.CardView

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.notascortesisu.notascortesisu.Model.Curso
import br.com.notascortesisu.notascortesisu.Model.NotaCorte
import br.com.notascortesisu.notascortesisu.Present.ViewHolder.CardCursoVH
import br.com.notascortesisu.notascortesisu.Present.ViewHolder.CardNotaCorteVH
import br.com.notascortesisu.notascortesisu.R

class NotaCorteAdapter(private val context: Context, private var listCardNotaCorte: MutableList<NotaCorte>) : RecyclerView.Adapter<CardNotaCorteVH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardNotaCorteVH {
        val view = LayoutInflater.from(context).inflate(R.layout.notas_corte_item,parent,false)
        return CardNotaCorteVH(view);
    }



    override fun getItemCount() = listCardNotaCorte.size

    override fun onBindViewHolder(holder: CardNotaCorteVH, position: Int) {
        holder.bindView(listCardNotaCorte[position])
    }

   // override fun getItemViewType(position: Int): Int {
     //   super.getItemViewType(position)
       // if(position)

   // }

}