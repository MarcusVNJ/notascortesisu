package br.com.notascortesisu.notascortesisu.View


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.notascortesisu.notascortesisu.Present.DetalhesNotaCortePst
import br.com.notascortesisu.notascortesisu.R
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds

import kotlinx.android.synthetic.main.activity_detalhes_nota_corte.*

class DetalhesNotaCorteActivity : AppCompatActivity() {

    private lateinit var notaCortePst : DetalhesNotaCortePst

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhes_nota_corte)

        notaCortePst = DetalhesNotaCortePst(this)

       // MobileAds.initialize(this,"ca-app-pub-3940256099942544~3347511713")

       // banner.loadAd(AdRequest.Builder().build())
    }
}
