package br.com.notascortesisu.notascortesisu.Present.ViewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import br.com.notascortesisu.notascortesisu.Model.Curso
import kotlinx.android.synthetic.main.notas_curso_item.view.*

class CardCursoVH(itemView:View) : RecyclerView.ViewHolder(itemView) {

    private val cursoNota = itemView.notaCurso
    private val nomeCurso = itemView.nomeCurso
    private val nomeIES = itemView.nomeIES

    fun bindView(curso: Curso){
        cursoNota.text = curso.nota.toString()
        nomeCurso.text = curso.nomeCurso
        nomeIES.text = curso.nomeIES
    }

}