package br.com.notascortesisu.notascortesisu.Present

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import br.com.notascortesisu.notascortesisu.Model.Curso
import br.com.notascortesisu.notascortesisu.Present.CardView.CursoAdapter
import br.com.notascortesisu.notascortesisu.Present.ViewHolder.PaginaPrincipalVH
import br.com.notascortesisu.notascortesisu.R


class PaginaPrincipalPst(activity: AppCompatActivity) {
    private var recyclerViewCursos: RecyclerView
    private var cursosList: MutableList<Curso> = mutableListOf(Curso(400, "Engenaharia de software", "UNIPAMPA"),Curso(500, "Engenaharia de batatas", "Batatolandia"),Curso(500, "Engenaharia de batatas", "Batatolandia"),Curso(500, "Engenaharia de batatas", "Batatolandia"),Curso(500, "Engenaharia de batatas", "Batatolandia"),Curso(500, "Engenaharia de batatas", "Batatolandia"),Curso(500, "Engenaharia de batatas", "Batatolandia"),Curso(500, "Engenaharia de batatas", "Batatolandia"),Curso(500, "Engenaharia de batatas", "Batatolandia"),Curso(500, "Engenaharia de batatas", "Batatolandia"))
    private var cursoAdapter: CursoAdapter
    private var paginaPrincipalVH: PaginaPrincipalVH

    init {
        this.recyclerViewCursos = activity.findViewById(R.id.listaCursos)
        this.cursoAdapter = CursoAdapter (activity.baseContext,cursosList)
        this.recyclerViewCursos.adapter = cursoAdapter
        this.recyclerViewCursos.layoutManager = LinearLayoutManager(activity.applicationContext)
        this.recyclerViewCursos.smoothScrollToPosition(cursosList.size)
        this.paginaPrincipalVH = PaginaPrincipalVH(activity)
    }




}